#include "test_sample_functions.h"

int main(int argc, char **argv) {
	Parameters parameters;
	parseArguments(argc, argv, &parameters);
	parameters.numberOfSamples = (parameters.time + parameters.delta - (parameters.delta / 10.))
		/ parameters.delta;

	// Initialize a matrix to store the sample values
	double **samplesMatrix = (double **) malloc(parameters.numberOfSamples * sizeof(double *));
	for (int i=0; i<parameters.numberOfSamples; ++i) {
		samplesMatrix[i] = (double *) malloc(4 * sizeof(double));
	}

	// Saves information about the experiment to the output file
	{
		FILE *outputFile;
		outputFile = fopen(parameters.outputFilename, "w");

		if (outputFile == NULL) {
			printf("Error while opening the output file.\n");
			exit(EXIT_FAILURE);
		}

		fprintf(outputFile, "Sampling experiment will run for %f seconds\n"\
			"Sampling period is set to %f seconds.\n"\
			"Number of samples should be %d.\n\n",
			parameters.time, parameters.delta, parameters.numberOfSamples);

		fclose(outputFile);
	}

	printf(ANSI_COLOR_BLUE "----- STARTING EXPERIMENT\n" ANSI_COLOR_RESET);

	testSampling(&samplesMatrix, parameters);

	printf(ANSI_COLOR_YELLOW "----- WRITING RESULTS TO OUTPUT FILE\n" ANSI_COLOR_RESET);

	// Saves results to the output file
	saveSamplesToFile(parameters.outputFilename, samplesMatrix, parameters.numberOfSamples);

	printf(ANSI_COLOR_GREEN "----- DONE\n" ANSI_COLOR_RESET);
}
