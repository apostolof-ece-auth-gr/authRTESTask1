# Real Time and Embedded Systems Task #1, 2019, AUTH
> Sampling accuracy testing using minimum energy possible

This is an *experimental* application developed as part of the course "Real Time and Embedded Systems" assignment, that took place in the Department of Electrical & Computer Engineering at Aristotle University of Thessaloniki in 2018-2019.

The goal is to test the accuracy of sampling in embedded systems using different techniques, while trying to minimize the energy consumption of the system. To this end, two approaches have been implemented and tested. The results are presented in the report pdf.

---

## Execution

To execute the code, you first need to compile it using:
```sh
make
```
and then run using:
```sh
./test_sample [-t time] [-d delta] [-o output]
```
where:

 1. **time** is the total duration of the sampling in seconds
 2. **delta** is the sampling period in seconds
 3. **output** is the output filename and path

all parameters are optional and have default values.

---

## Status

As of the completion of the project, it will NOT be maintained. The aim is to ensure its originality to the day it was delivered. By no means should it ever be considered stable or safe to use, as it may contain incomplete parts, critical bugs and security vulnerabilities.

---

## Support

Reach out to me:

- [apostolof's email](mailto:apotwohd@gmail.com "apotwohd@gmail.com")

---

## License

[![Beerware License](https://img.shields.io/badge/license-beerware%20%F0%9F%8D%BA-blue.svg)](https://gitlab.com/apostolof-ece-auth-gr/authRTESTask1/blob/master/LICENSE.md)