SHELL := /bin/bash

# ============================================
# COMMANDS

CC = gcc -std=gnu99
RM = rm -f
CFLAGS_DEBUG=-O0 -ggdb3 -Wall -Werror -pedantic -D_FORTIFY_SOURCE=2 -fasynchronous-unwind-tables -grecord-gcc-switches -Werror=implicit-function-declaration -I.
CFLAGS=-O2 -Wall -Werror -pedantic -I.

OBJ=test_sample.o test_sample_functions.o
DEPS=test_sample_functions.h

# ==========================================
# TARGETS

EXECUTABLES = test_sample.out

.PHONY: all clean

all: $(EXECUTABLES)

# ==========================================
# DEPENDENCIES (HEADERS)

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

.PRECIOUS: $(EXECUTABLES) $(OBJ)

# ==========================================
# EXECUTABLE (MAIN)

$(EXECUTABLES): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	$(RM) *.o *~ $(EXECUTABLES)
