#ifndef TEST_SAMPLE_FUNCTIONS_H	/* Include guard */
#define TEST_SAMPLE_FUNCTIONS_H

/* ===== INCLUDES ===== */

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

/* ===== DEFINITIONS ===== */

//Colors used for better console output formating.
#define ANSI_COLOR_RED     "\x1B[31m"
#define ANSI_COLOR_GREEN   "\x1B[32m"
#define ANSI_COLOR_YELLOW  "\x1B[33m"
#define ANSI_COLOR_BLUE    "\x1B[34m"
#define ANSI_COLOR_CYAN    "\x1B[36m"
#define ANSI_COLOR_RESET   "\x1B[0m"

/* ===== CONSTANTS DEFINITION ===== */

// Constant strings that store the command line options available.
extern const char *ARGUMENT_TIME;
extern const char *ARGUMENT_DELTA;
extern const char *ARGUMENT_OUTPUT_FILENAME;
// Default filename used for the output.
extern char *DEFAULT_OUTPUT_FILENAME;

/* ===== STRUCTURES ===== */

// Structures used in gettimeofday
extern struct timeval startwtime, endwtime;

// A data structure to conveniently hold the algorithm's parameters.
typedef struct parameters {
	double time, delta;
	char *outputFilename;
	int numberOfSamples;
} Parameters;

/* ===== FUNCTION DEFINITIONS ===== */

// Function that implements the sampling experiment.
void testSampling(double ***samplesMatrix, Parameters parameters);

void handle_alarm(int sig);

// Function validUsage outputs the correct way to use the program with command line arguments.
void validUsage(char *programName);

// Function checkIncrement is a helper function used in parseArguments (see bellow).
int checkIncrement(int previousIndex, int maxIndex, char *programName);

// Function parseArguments parses command line arguments.
void parseArguments(int argumentCount, char **argumentVector, Parameters *parameters);

// Function saveSamplesToFile appends the sample matrix "samplesMatrix" to the file with the
// filename supplied in the arguments.
void saveSamplesToFile(char *filename, double **samplesMatrix, int matrixRows);

#endif	// TEST_SAMPLE_FUNCTIONS_H
